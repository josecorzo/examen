'use strict'

const mongoose = require('mongoose');

class DB{
    constructor(USER_DB, PASSWORD_DB, CLUSTER, DATABASE){
        this.destiny = `mongodb+srv://${USER_DB}:${PASSWORD_DB}${CLUSTER}/${DATABASE}?retryWrites=true&w=majority`;
    }

    async connect(){
        try{
            await mongoose.connect(this.destiny, {
                useCreateIndex: true,    
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false
            });

            console.log("Database is connected successfully");
        }
        catch(error){
            console.log("Something went wrong trying to establish a connection with the database :(");
            console.log(error);
        }
    }
}

module.exports = DB;