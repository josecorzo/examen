'use strict'

const express = require("express");
const database = require("../config/database");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");

class Server{
    constructor(){
        this.app = express();
        this.db = new database('admin','xQC0dIHCjwsqc6jz','@cluster0.r9608.mongodb.net','db_tienda');
        this.port = process.env.PORT || 3900;
    }

    middlewares(){
        this.app.use(express.static(path.resolve(__dirname, '../public')));
        this.app.use(cors());
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(bodyParser.json());
    }

    routing(){
        this.app.use('/api', require("../routes/api"));
    }

    initialize(){
        try{
            this.middlewares();
            this.routing();
    
            this.app.listen(this.port, () => {
                console.log(`Sever running on port: ${this.port}`);
                this.db.connect();
            });
        }
        catch(error){
            console.log("Something went wrong trying to initialize the server.");
            console.log(error);
        }
    }
}

module.exports = new Server();