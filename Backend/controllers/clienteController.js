'use strict'

const Cliente = require('../models/cliente');
const Venta = require('../models/venta');

const controller = {

  save: async(req,res) => {

    try {
      
      let {nombre, apellidos} = req.body;

      let user = await Cliente.find().sort('createdAt').limit(1);

      let folio = user.length > 0 ? user[0].folio : 1;  
      folio++;

      if(folio.toString().length == 1){
        folio = "000"+folio;
      }
      else if(folio.toString().length == 2){
        folio = "00"+folio;
      }
      else if(folio.toString().length == 3){
        folio = "0"+folio;
      }

      let cliente = await Cliente.create({folio, nombre, apellidos});

      return res.send({
        status: 200,
        cliente
      });

    } 
    catch (error) {
      return res.send({
        status: 500,
        message: 'Ocurrio un error al tratar de guardar el cliente'
      });
    }

  },

  getClientes: async(req,res) => {
    await Cliente.find({}).exec((err, clientes) => {

      if(err){
        return res.send({
          status: 500,
          message: "Error al devolver los datos"
        });
      }

      if (!clientes) {
        return res.send({
          status: 404,
          message: "No hay clientes que mostrar",
        });
      }

      return res.send({
        status: 200,
        clientes
      });
    })
    
  },

  getCliente: async(req, res)=>{

    const clienteId = req.params.id;

    if(!clienteId || clienteId == null){
        return res.status(404).send({
          status: "error",
          message: "No existe el cliente",
        });
    }

    Cliente.findById(clienteId, (err,cliente) => {
      if (err || !cliente) {
        return res.status(404).send({
          status: "error",
          message: "No existe el cliente",
        });
      }

      //res
      return res.status(404).send({
        status: "success",
        cliente
      });
    });
  },

  updateCliente: async(req, res) =>{
   try {
      
      let {nombre, apellidos} = req.body;

      let cliente = await Cliente.findByIdAndUpdate(req.params.id, {nombre, apellidos}, {new: true});

      return res.send({
        status: 200,
        cliente
      });

    } 
    catch (error) {
      return res.send({
        status: 500,
        message: 'Ocurrio un error al tratar de actualizar el cliente'
      });
    }
  },
  deleteCliente: async(req, res) =>{
   try {
      await Venta.find({cliente: req.params.id}).deleteMany();
      await Cliente.findByIdAndDelete(req.params.id);

      return res.send({
        status: 200
      });

    } 
    catch (error) {
      return res.send({
        status: 500,
        message: 'Ocurrio un error al tratar de eliminar el cliente'
      });
    }
  }
};

module.exports = controller;