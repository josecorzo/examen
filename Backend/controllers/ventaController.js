'use strict'

const validator = require('validator');
const Venta = require('../models/venta');

const controller = {

  save: async(req,res) => {

    try {

      let articles = req.body;

      let lastSale = await Venta.find().sort('-createdAt').limit(1);

      let folioVenta = lastSale.length > 0 ? lastSale[0].folioVenta : 1;  
      folioVenta++;

      if(folioVenta.toString().length == 1){
        folioVenta = "000"+folioVenta;
      }
      else if(folioVenta.toString().length == 2){
        folioVenta = "00"+folioVenta;
      }
      else if(folioVenta.toString().length == 3){
        folioVenta = "0"+folioVenta;
      }

      let sale = [];

      /*
       id,
        nombre,
        marca,
        cantidad: 1,
        precio,
        importe:
     */

      /*
      id: { type: Schema.Types.ObjectId, ref: "Articulo", autopopulate: true},
      cantidad: Number,
      importe: Schema.Types.Decimal128,
      */

      await articles.map((article) => {
        let {id, cantidad, importe} = article;
        sale.push({id, cantidad, importe});
      });

      let total = Object.values(articles).reduce((a, {importe}) => a + importe, 0);

      let venta = await Venta.create({folioVenta, cliente: req.params.id, articulos: sale, total});

      return res.send({
        status: 200,
        venta
      });
    } catch (error) {console.log(error);
      return res.send({
        status: 500,
        message: 'Ocurrio un error al tratar de guardar la venta'
      });
    }
    
  },

  getVentas: async(req,res) => {
    await Venta.find().exec((err, ventas) => {

      if(err){
        return res.send({
          status: 500,
          message: "Error al devolver los datos de la venta"
        });
      }

      if (!ventas) {
        return res.send({
          status: 404,
          message: "No hay ventas que mostrar",
        });
      }

      return res.send({
        status: 200,
        ventas
      });
    })
    
  },

  getVenta:(req, res)=>{
    const ventaId = req.params.id;

    if (!ventaId || ventaId == null) {
      return res.send({
        status: 404,
        message: "No existe el articulo",
      });
    }

    Venta.findById(ventaId, (err, venta) => {
      if (err || !venta) {
        return res.send({
          status: 404,
          message: "No existe el venta",
        });
      }

      return res.send({
        status: 200,
        venta
      });
    });
  },

  updateVenta: (req, res) =>{
   //recoger el id
    var ventaId = req.params.id;
   //recoger los datos del put
    var params = req.body;
   //validar datos
    try {
      var validate_folioVenta = !validator.isEmpty(params.folioVenta);
      var validate_cliente = !validator.isEmpty(params.cliente);
      var validate_articulo = !validator.isEmpty(params.articulo);
      var validate_cantidad = !validator.isEmpty(params.cantidad);
      var validate_importe = !validator.isEmpty(params.importe);
      var validate_total = !validator.isEmpty(params.total);
    } catch (error) {
       return res.status(200).send({
         status: "error",
         message: "Faltan datos por enviar",
       });
    }

    if (
      validate_folioVenta &&
      validate_cliente &&
      validate_articulo &&
      validate_cantidad &&
      validate_importe &&
      validate_total
    ) {
      //findandupdate
      Venta.findByIdAndUpdate(
        { _id: ventaId },
        params,
        { new: true },
        (err, ventaUpdate) => {
          if (err) {
            return res.status(500).send({
              status: "error",
              message: "Error al actualizar",
            });
          }

          if (!ventaUpdate) {
            return res.status(404).send({
              status: "error",
              message: "No existe el articulo",
            });
          }

          return res.status(404).send({
            status: "success",
            venta: ventaUpdate,
          });
        }
      );
    } else {
      //res
      return res.status(200).send({
        status: "error",
        message: "La validación no es correcta",
      });
    }
  }
};

module.exports = controller;