'use strict'

const validator = require('validator');
const Articulo = require('../models/articulo');

const controller = {

  save: async(req,res) => {

    try {

      let {nombre, marca, precio} = req.body;

      let article = await Articulo.find().sort('-createdAt').limit(1);
      
      let articulo = await Articulo.create({nombre, marca, precio})

      return res.send({
        status:200,
        articulo
      });
    } catch (error) {
      return res.send({
        status: 500,
        message: 'Ocurrio un error al tratar de guardar el articulo'
      });
    }
  },

  getArticulos: async(req,res) => {
   await Articulo.find().exec((err, articulos) => {

      if(err){
        return res.send({
          status: 500,
          message: "Error al devolver los datos del articulo"
        });
      }

      if (!articulos) {
        return res.send({
          status: 404,
          message: "No hay articulos que mostrar",
        });
      }


      return res.send({
        status: 200,
        articulos
      });
    })
    
  },

  getArticulo:(req, res)=>{

    const articuloId = req.params.id;

    if (!articuloId || articuloId == null) {
      return res.send({
        status: 404,
        message: "No existe el articulo"
      });
    }

    Articulo.findById(articuloId, (err, articulo) => {
      if (err || !articulo) {
        return res.send({
          status: 404,
          message: "No existe el articulo"
        });
      }

      return res.send({
        status: 200,
        articulo,
      });
    });
  },

   updateArticulo: async(req, res) =>{
   try {
      
      let {nombre, marca, precio} = req.body;

      let articulo = await Articulo.findByIdAndUpdate(req.params.id, {nombre, marca, precio}, {new: true});

      return res.send({
        status: 200,
        articulo
      });

    } 
    catch (error) {
      return res.send({
        status: 500,
        message: 'Ocurrio un error al tratar de actualizar el articulo'
      });
    }
  }

};

module.exports = controller;