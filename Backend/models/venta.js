'use strict';

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const moment = require("moment-timezone");

const VentaSchema = Schema({
  folioVenta: String,
  cliente: { type: Schema.Types.ObjectId, ref: "Cliente", autopopulate: true},
  articulos: [
    {
      id: { type: Schema.Types.ObjectId, ref: "Articulo", autopopulate: true},
      cantidad: Number,
      importe: Schema.Types.Decimal128,
    }
  ],
  total: Schema.Types.Decimal128,
},
{
  collection: 'Venta',
  timestamps: true
});

VentaSchema.method('toJSON', function(){
  const { __v, _id, updatedAt, importe, total, cliente, ...object} = this.toObject();
  object.id = _id;
  // object.importe = importe.toString();
  object.total = total.toString();
  object.createdAt = moment(object.createdAt).tz('America/Monterrey').format('DD/MM/YYYY');

  delete cliente.__v;
  delete cliente.createdAt;
  delete cliente.updatedAt;

  cliente.id = cliente._id;
  delete cliente._id;

  object.cliente = cliente;

  // delete articulo.__v;
  // delete articulo.createdAt;
  // delete articulo.updatedAt;

  // articulo.id = articulo._id;
  // delete articulo._id;
  // articulo.precio = articulo.precio.toString();

  // object.articulo = articulo;

  return object;
});

VentaSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('Venta', VentaSchema);