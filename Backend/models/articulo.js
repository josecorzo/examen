'use strict';

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ArticuloSchema = Schema({
  nombre: String,
  marca: String,
  precio: Schema.Types.Decimal128,
}, 
{
  collection: 'Articulo',
  timestamps: true
});

ArticuloSchema.method('toJSON', function(){
  const { __v, _id, createdAt, updatedAt, precio, ...object} = this.toObject();
  object.id = _id;
  object.precio = precio.toString();
  return object;
});

module.exports = mongoose.model('Articulo', ArticuloSchema);
