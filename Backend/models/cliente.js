'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClienteSchema = Schema({
  folio: String,
  nombre: String,
  apellidos: String,
},
{
  collection: 'Cliente',
  timestamps: true
});

ClienteSchema.method('toJSON', function(){
  const { __v, _id, createdAt, updatedAt, ...object} = this.toObject();
  object.id = _id;
  return object;
});

module.exports = mongoose.model('Cliente', ClienteSchema);