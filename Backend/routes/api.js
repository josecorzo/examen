'use strict'

const express = require('express');
const router = express.Router();
require("express-group-routes");

const ArticuloController = require('../controllers/articuloController');
const ClienteController = require('../controllers/clienteController');
const VentaController = require('../controllers/ventaController');

router.group("/articulo", (articulo) => {
  articulo.post('/', ArticuloController.save);
  articulo.get('/', ArticuloController.getArticulos);
  articulo.get('/:id', ArticuloController.getArticulo);
  articulo.put("/:id", ArticuloController.updateArticulo);
});

router.group("/cliente", (cliente) => {
  cliente.post('/', ClienteController.save);
  cliente.get('/', ClienteController.getClientes);
  cliente.get('/:id', ClienteController.getCliente);
  cliente.put("/:id", ClienteController.updateCliente);
  cliente.delete("/:id", ClienteController.deleteCliente);
});

router.group("/venta", (venta) => {
  venta.post('/:id', VentaController.save);
  venta.get('/', VentaController.getVentas);
  venta.get('/:id', VentaController.getVenta);
  venta.put('/:id', VentaController.updateVenta);
  // venta.delete('/:id', VentaController.deleteVenta);
});

module.exports = router;