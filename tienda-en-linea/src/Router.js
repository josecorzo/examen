import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from './components/home.js'
import Venta from './components/ventas/nueva-venta.js'
import Header from './components/header.js'
import Cliente from './components/clientes/cliente.js'
import Articulo from './components/articulos/articulo'


class Router extends Component{
  render(){
    return (
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/venta" exact component={Venta} />
          <Route path="/clientes" exact component={Cliente}></Route>
          <Route path="/articulos" exact component={Articulo}></Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Router;