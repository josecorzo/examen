import React from 'react';
import { Navbar, Nav, NavDropdown, Badge } from 'react-bootstrap'
import { NavLink } from "react-router-dom";
import './assets/header.css'

import moment from 'moment-timezone';
import 'moment/locale/es';
moment.locale('es');
moment.tz('America/Mexico_City');

const Header = () => {
    const datetime = moment().format('dddd D [de] MMMM [del] YYYY');

    return (
      <>
        <Navbar id="home" collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Brand as={NavLink} to="/">
            Sistema de tienda en linea 2.0
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <NavDropdown title="Menú">
                <NavDropdown.Item as={NavLink} to="/">
                  Lista de ventas
                </NavDropdown.Item>

                <NavDropdown.Item as={NavLink} to="/venta">
                  Nueva venta
                </NavDropdown.Item>

                <NavDropdown.Item as={NavLink} to="/clientes">
                  Clientes
                </NavDropdown.Item>

                <NavDropdown.Item as={NavLink} to="/articulos">
                  Articulos
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <h5>
              <Badge variant="info">{datetime}</Badge>
            </h5>
          </Navbar.Collapse>
        </Navbar>
      </>
    );
}

export default Header;