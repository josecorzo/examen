import React, { useState, useEffect } from 'react';
import axios from  '../utils/axios';
import { Table,Button,Container,Form,Row,Col,Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Loader from '../utils/Loader';
import Swal from 'sweetalert2';

const Venta = () =>  {
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  const [clientes, setClientes] = useState([]);
  const [clientSell, setClientSell] = useState({});
  const [articulos, setArticulos] = useState([]);
  const [articulosVenta, setArticulosVenta] = useState([]);

    useEffect(async() => {
      try{
        let clienteAPI = await axios.get('cliente');
      
        if(clienteAPI.data.status === 200){
          setClientes(clienteAPI.data.clientes);
        }
        else if(clienteAPI.data.status === 500){
          Swal.fire({
            icon: 'error',
            title: clienteAPI.data.message,
          });
        }
  
        let articuloAPI = await axios.get('articulo');
        
        if(articuloAPI.data.status === 200){
          setArticulos(articuloAPI.data.articulos);
        }
        else if(articuloAPI.data.status === 500){
          Swal.fire({
            icon: 'error',
            title: articuloAPI.data.message,
          });
        }
      }
      catch(error){
        Swal.fire({
          icon: 'error',
          title: 'Hubo un error al obtener los clientes y los artículos',
          confirmButtonText: 'Aceptar!'
        });
      }
	setLoading(false);
    }, [])
    
    const [showClientesModal, setShowClientesModal] = useState(false);
    const [showArticulosModal, setShowArticulosModal] = useState(false);

    const selectClient = (id) => {
      setClientSell(clientes.find(c => c.id === id));
      setShowClientesModal(false);
    };

    const selectArticle = (idSelected) => {
      let {id, nombre, marca, precio} = articulos.find(a => a.id === idSelected);
      let article = {
        id,
        nombre,
        marca,
        cantidad: 1,
        precio,
        importe: parseFloat(precio)
      }

      let articulosVentaCopy = [...articulosVenta];
      articulosVentaCopy.push(article);
      setArticulosVenta(articulosVentaCopy);
    };

    const handleUpdateQuantity = ({target}) => {
      let articulosVentaCopy = [...articulosVenta];
      let index = articulosVentaCopy.findIndex(a => a.id === target.name);
      articulosVentaCopy[index].cantidad = target.value;
      articulosVentaCopy[index].importe = parseFloat(articulosVentaCopy[index].precio * target.value);
      setArticulosVenta(articulosVentaCopy);
    }

    const handleDelete = (id) => {

      Swal.fire({
      title: 'Está seguro de eliminar este registro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar!',
      cancelButtonText: ' Cancelar!',
    }).then(async(result) => {
      if (result.isConfirmed) {
            setLoading(true);


      let articulosVentaCopy = [...articulosVenta];
      let index = articulosVentaCopy.findIndex(a => a.id === id);
      articulosVentaCopy.splice(index, 1);
      setArticulosVenta(articulosVentaCopy);

      
            setLoading(false);
      }
    })
    }

    const handleSubmit = async() => {
      if(!clientSell?.id && articulosVenta?.length === 0){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo salio mal!'
        });
      }
      else{
        setLoading(true);

        let {data} = await axios.post(`venta/${clientSell.id}`, articulosVenta);

        if(data.status === 200){
          Swal.fire({
            icon: 'success',
            title: 'Bien hecho tu venta ha sido generada correctamente!',
            confirmButtonText:'Aceptar'
          });

          history.push('/')
        }
        else{
          Swal.fire({
            icon: 'error',
            title: data.msg,
            confirmButtonText:'Aceptar'
          });
        }

        setLoading(false);
      }
    }
     
    return (
      <>
        {
          loading &&
          <Loader loading={loading}/>
        }

        <Modal show={showClientesModal} onHide={() => setShowClientesModal(false)} animation={false}>
          <Modal.Header closeButton>

            <Modal.Title>Seleccionar cliente</Modal.Title>

          </Modal.Header>

          <Modal.Body>
            <Table id="sel-venta" responsive>
              <thead>
                <tr>
                  <th>Folio</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                </tr>
              </thead>
              <tbody>
                 {
                  clientes?.map((cliente, key) => {
                    return <tr key={key} onClick={() => selectClient(cliente.id)}>
                            <td>{cliente.folio}</td>
                            <td>{cliente.nombre}</td>
                            <td>{cliente.apellidos}</td>
                          </tr>
                  })
                }
              </tbody>
            </Table>
          </Modal.Body>

          <Modal.Footer>

            <Button variant="outline-secondary" onClick={() => setShowClientesModal(false)}>
              Cerrar
            </Button>

            <Button variant="outline-primary" onClick={() => setShowClientesModal(false)}>
              Aceptar
            </Button>
            
          </Modal.Footer>
        </Modal>

        <Modal show={showArticulosModal} onHide={() => setShowArticulosModal(false)} animation={false}>
          <Modal.Header closeButton>

            <Modal.Title>Seleccionar artículo</Modal.Title>

          </Modal.Header>

          <Modal.Body>
            <Table id="sel-venta" responsive>
              <thead>
                <tr>
                  <th>Articulo</th>
                  <th>Marca</th>
                  <th>Precio</th>
                </tr>
              </thead>
              <tbody>
                 {
                articulos?.map((articulo, key) => {
                  return <tr key={key} onClick={() => selectArticle(articulo.id)}>
                          <td>{articulo.nombre}</td>
                          <td>{articulo.marca}</td>
                          <td>${new Intl.NumberFormat().format(articulo.precio).toString().replace('.', ',')}</td>
                          
                        </tr>
                })
              }
              </tbody>
            </Table>
          </Modal.Body>

          <Modal.Footer>

            <Button variant="outline-secondary" onClick={() => setShowArticulosModal(false)}>
              Cerrar
            </Button>

            <Button variant="outline-primary" onClick={() => setShowArticulosModal(false)}>
              Aceptar
            </Button>
            
          </Modal.Footer>
        </Modal>

        {
          !loading &&
          <Container fluid="md">
          <Form className="mb-3 mt-3">
            <Form.Row className="align-items-center">
              <Col xs="auto">
                <Form.Label>
                  <h4>Cliente</h4>
                </Form.Label>
              </Col>

              <Col xs="auto">
                <Form.Control className="mb-2 mr-sm-2" placeholder="Nombre cliente" readOnly value={clientSell?.hasOwnProperty('folio') ? clientSell.folio + ' ' + clientSell.nombre : ''}/>
              </Col>

              <Col xs="auto">
                <Button variant="outline-primary" className="mb-2 mr-3" onClick={() => setShowClientesModal(true)}>
                  Agregar cliente
                </Button>

                <Button variant="outline-primary" className="mb-2" onClick={() => setShowArticulosModal(true)}>
                  Agregar artículo
                </Button>
              </Col>
            </Form.Row>
          </Form>
          <hr />

          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>Descripción Articulo</th>
                <th>Marca</th>
                <th>Cantidad</th>
                <th>Precio</th>
                <th>Importe</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              {
                articulosVenta?.map((articulo, key) => {
                  return <tr key={key}>
                          <td>{articulo.nombre}</td>
                          <td>{articulo.marca}</td>
                          <td><input className="quantity" type="number" name={articulo.id} value={articulo.cantidad} min="1" onChange={handleUpdateQuantity}/></td>
                          <td>${new Intl.NumberFormat().format(articulo.precio).toString().replace('.', ',')}</td>
                          <td>${new Intl.NumberFormat().format(articulo.importe).toString().replace('.', ',')}</td>
                          <td>
                            <Button variant="outline-danger" size="sm" onClick={() => handleDelete(articulo.id)}>Eliminar</Button>
                          </td>
                        </tr>
                })
              }
            </tbody>
          </Table>

          <hr />

          <Col md={5} className="float-right">
            <Table striped bordered hover size="sm">
              <tbody>
                <tr>
                  <th>Importe</th>
                  <td align="center">${new Intl.NumberFormat().format(Object.values(articulosVenta).reduce((a, {importe}) => a + importe, 0)).toString().replace('.', ',')}</td>
                </tr>
                <tr>
                  <th>IVA</th>
                  <td align="center">${new Intl.NumberFormat().format((Object.values(articulosVenta).reduce((a, {importe}) => a + importe, 0) * 0.16)).toString().replace('.', ',')}</td>
                </tr>
                <tr>
                  <th>Total</th>
                  <td align="center">${new Intl.NumberFormat().format((Object.values(articulosVenta).reduce((a, {importe}) => a + importe, 0) * 0.16) + Object.values(articulosVenta).reduce((a, {importe}) => a + importe, 0)).toString().replace('.', ',')}</td>
                </tr>
              </tbody>
            </Table>
            
              <Button className="float-right" variant="outline-success" onClick={() => handleSubmit()}>Guardar</Button>
            
          </Col>
        </Container>
        }
      </>
    );
  
}

export default Venta;
