import React from 'react';
import { Spinner } from 'react-bootstrap';

const Loader = ({loading, children}) => {

  if(!loading){
    return {children}
  }

  return(
    <div className="loader-container">
        <div className="loader-items">
            <p className="text-center title-mg fz-18">Cargando</p>
            <div className="img">
              <Spinner animation="border" />
            </div>
        </div>
    </div>
  )
}

export default Loader;