import React, { useEffect, useState } from 'react'
import axios from '../utils/axios';
import { Container,Card,Button,Form,Row,Col,Table,ButtonGroup,Alert } from 'react-bootstrap';
import Loader from '../utils/Loader';
import Swal from 'sweetalert2';

const Articulo = () => {
  const [loading, setLoading] = useState(true);
  const [articulos, setArticulos] = useState([]);
  const [articulo, setArticulo] = useState({
    id: '',
    nombre: '',
    marca: '',
    precio: ''
  });
  const [errors, setErrors] = useState(null);

     useEffect(async() => {
      try{
        let {data} = await axios.get('articulo');
      
        if(data.status === 200){
          setArticulos(data.articulos);
        }  
      }
      catch(error){
        Swal.fire({
          icon: 'error',
          title: 'Hubo un error al obtener los articulos',
          confirmButtonText: 'Aceptar!'
        });
      }
      setLoading(false);
    }, []);

    const handleChange = ({target}) => {

      if((target.name === 'nombre' || target.name === 'marca') && !/^$|[a-záéíóúü\s]$/i.test(target.value)){
        setErrors('Sólo se aceptan letras');
        setTimeout(() => {
          setErrors(null)
        }, 1000)
      }
      else if(target.name === 'precio' && !/^$|[0-9]$/i.test(target.value)){
        setErrors('Sólo se aceptan números');
        setTimeout(() => {
          setErrors(null)
        }, 1000)
      }
      else{
        setArticulo({
          ...articulo,
          [target.name]: target.value
        })
      }
    }
    const selectArticulo = (idSelected) => {
    let {id, nombre, marca, precio} = articulos.find(c => c.id === idSelected);
    setArticulo({id, nombre, marca, precio});
  }

  const cancelEdit = () => {
    setArticulo({
      id: '',
      nombre: '',
      marca: '',
      precio: ''
    });
  }

  const handleSubmit = async() => {
    if(articulo.nombre.length === 0 || articulo.marca.length === 0 || articulo.precio.length === 0){
      setErrors('El nombre la marca y el precio son obligatorios');
      setTimeout(() => {
        setErrors(null)
      }, 5000)
    }
    else{
      setLoading(true);

      if(!articulo.id){
        let {data} = await axios.post('articulo', articulo);

        if(data.status === 200){
          let articulosCopy = [...articulos];
          articulosCopy.push(data.articulo);
          setArticulos(articulosCopy);
          setArticulo({
            id: '',
            nombre: '',
            marca: '',
            precio:''
          });
          Swal.fire({
            icon: 'success',
            title: 'Articulo guardado correctamente.',
            confirmButtonText: 'Aceptar!'

          });
        }
        else if(data.status === 500) {
          Swal.fire({
            icon: 'error',
            title: data.message,
          });
        }
      }
      else{
        let {data} = await axios.put(`articulo/${articulo.id}`, articulo);
        
        if(data.status === 200){
          let articulosCopy = [...articulos];
          let index = articulos.findIndex(c => c.id === articulo.id);
          articulosCopy[index] = data.articulo;
          setArticulos(articulosCopy);
          setArticulo({
            id: '',
            nombre: '',
            marca: '',
            precio:''
          });
          Swal.fire({
            icon: 'success',
            title: 'Articulo actualizado correctamente.',
            confirmButtonText: 'Aceptar!'
          });
        }
        else if(data.status === 500) {
          Swal.fire({
            icon: 'error',
            title: data.message,
          });
        }
      }

      setLoading(false);
    }
  }

 

  return(
  <>
     {
        loading &&
        <Loader loading={loading}/>
      }
      {
        !loading &&
    <Container>
     <Row>
       <Col lg={4}>
          <Card className="mt-5">
            <Card.Header as="h5">Sección articulos</Card.Header>
            <Card.Body>
              <Card.Title>
                {
                      !articulo.id ? 'Agregar nuevo registro' : 'Editar registro'
                    }
              </Card.Title>
                <Form>
                  <Form.Group>
                    <Form.Label>Nombre</Form.Label>
                    <Form.Control name="nombre" type="text" placeholder="Nombre" value={articulo.nombre} onChange={handleChange}/>
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Marca</Form.Label>
                    <Form.Control name="marca" type="text" placeholder="Marca" value={articulo.marca} onChange={handleChange}/>
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Precio</Form.Label>
                    <Form.Control name="precio" type="text" placeholder="Precio" value={articulo.precio} onChange={handleChange}/>
                  </Form.Group>
                   { 
                        errors &&
                        <Alert variant="danger">
                          {errors}
                        </Alert>
                      }

                      <Button variant="outline-primary" onClick={() => handleSubmit()}>
                        {
                          !articulo.id ? 'Guardar' : 'Actualizar'
                        }
                      </Button>

                        {
                          articulo.id &&
                          <Button variant="outline-danger" className="float-right" onClick={() => cancelEdit()}>Cancelar</Button>
                        }
                </Form>
            </Card.Body>
          </Card>
        </Col>

        <Col>
         <Table responsive className="mt-5">
            <thead>
              <tr>
                <th>Descripción</th>
                <th>Marca</th>
                <th>Precio</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
               {
                    articulos?.map((articulo, key) => {
                      return <tr key={key}>
                              <td>{articulo.nombre}</td>
                              <td>{articulo.marca}</td>
                              <td>${new Intl.NumberFormat().format(articulo.precio).toString().replace('.', ',')}</td>
                              <td align="center">
                                  <ButtonGroup size="sm">
                                    <Button variant="outline-warning" onClick={() => selectArticulo(articulo.id)}>Editar</Button>
                                  </ButtonGroup>
                                </td>
                            </tr>
                    })
                  }
              
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  }
  </>
  );

}
export default Articulo;