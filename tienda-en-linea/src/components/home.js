import React, { useEffect, useState }  from 'react';
import axios from './utils/axios';
import { Table,Container,Button } from 'react-bootstrap'
import { NavLink } from 'react-router-dom';
import Loader from './utils/Loader';
import Swal from 'sweetalert2';

const Home = () => {
    const [loading, setLoading] = useState(true);
    const [ventas, setVentas] = useState([]);

    useEffect(async() => {
      try{
        let {data} = await axios.get('venta');
      
        if(data.status === 200){
          setVentas(data.ventas);
        }
        else if(data.status === 500){
          Swal.fire({
            icon: 'error',
            title: data.message,
            confirmButtonText: 'Aceptar!'
          });
        }
      }
      catch(error){
        Swal.fire({
          icon: 'error',
          title: 'Hubo un error al obtener las ventas',
          confirmButtonText: 'Aceptar!'
        });
      }

      setLoading(false);
    }, []);
    return (
      <>
        {
          loading &&
          <Loader loading={loading}/>
        }

        {
          !loading &&
            <Container fluid="md">
              
              <Button variant="outline-success" className="mb-3 mt-3 float-right" as={NavLink} to="/venta">Nueva venta</Button>
              <Table striped bordered hover size="sm">
                <thead>
                  <tr>
                    <th>Folio venta</th>
                    <th>No. Cliente</th>
                    <th>Nombre</th>
                    <th>Total</th>
                    <th>Fecha</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    ventas?.map((venta, key) => {
                      return <tr key={key}>
                              <td>{venta.folioVenta}</td>
                              <td>{venta.cliente.folio}</td>
                              <td>{venta.cliente.nombre + ' ' + venta.cliente.apellidos}</td>
                              <td>${new Intl.NumberFormat().format(venta.total).toString().replace('.', ',')}</td>
                              <td>{venta.createdAt}</td>
                            </tr>
                    })
                  }
                </tbody>
              </Table>
            </Container>
      }
      </>
    );
}

export default Home;