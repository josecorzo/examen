import React, { useEffect, useState } from 'react';
import axios from '../utils/axios';
import { Container,Card,Button,Form,Row,Col,Table, ButtonGroup, Alert } from 'react-bootstrap';
import Loader from '../utils/Loader';
import Swal from 'sweetalert2';

const Cliente = () => {
  const [loading, setLoading] = useState(true);
  const [clientes, setClientes] = useState([]);
  const [cliente, setCliente] = useState({
    id: '',
    nombre: '',
    apellidos: ''
  });
  const [errors, setErrors] = useState(null);

    useEffect(async() => {
      try{
        let {data} = await axios.get('cliente');
      
        if(data.status === 200){
          setClientes(data.clientes);
        }  
      }
      catch(error){
        Swal.fire({
          icon: 'error',
          title: 'Hubo un error al obtener los clientes',
          confirmButtonText: 'Aceptar!'
        });
      }
      setLoading(false);
    }, []);


  const handleChange = ({target}) => {

    if(!/^$|[a-záéíóúü\s]$/i.test(target.value)){
      setErrors('Sólo se aceptan letras');
      setTimeout(() => {
        setErrors(null)
      }, 1000)
    }
    else{
      setCliente({
        ...cliente,
        [target.name]: target.value
      })
    }
  }

  const selectCliente = (idSelected) => {
    let {id, nombre, apellidos} = clientes.find(c => c.id === idSelected);
    setCliente({id, nombre, apellidos});
  }

  const cancelEdit = () => {
    setCliente({
      id: '',
      nombre: '',
      apellidos: ''
    });
  }

  const handleDelete = (id) => {

    Swal.fire({
      title: 'Esta seguro de eliminar este registro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar!',
      cancelButtonText: ' Cancelar!',
    }).then(async(result) => {
      if (result.isConfirmed) {
            setLoading(true);

            let {data} = await axios.delete(`cliente/${id}`);

            if(data.status === 200){
              let clientesCopy = [...clientes];
              let index = clientes.findIndex(c => c.id === id);
              clientesCopy.splice(index, 1);
              setClientes(clientesCopy);
              Swal.fire({
                  icon: 'success',
                  title: 'Cliente eliminado correctamente.',
                  confirmButtonText: 'Aceptar!'
                });
            }
            else if(data.status === 500) {
              Swal.fire({
                icon: 'error',
                title: data.message,
              });
            }

            setLoading(false);
      }
    })
  }

  const handleSubmit = async() => {
    if(cliente.nombre.length === 0 || cliente.apellidos.length === 0){
      setErrors('El nombre y apellidos son obligatorios');
      setTimeout(() => {
        setErrors(null)
      }, 5000)
    }
    else{
      setLoading(true);

      if(!cliente.id){
        let {data} = await axios.post('cliente', cliente);

        if(data.status === 200){
          let clientesCopy = [...clientes];
          clientesCopy.push(data.cliente);
          setClientes(clientesCopy);
          setCliente({
            id: '',
            nombre: '',
            apellidos: ''
          });
          Swal.fire({
            icon: 'success',
            title: 'Cliente guardado correctamente.',
            confirmButtonText: 'Aceptar!'

          });
        }
        else if(data.status === 500) {
          Swal.fire({
            icon: 'error',
            title: data.message,
          });
        }
      }
      else{
        let {data} = await axios.put(`cliente/${cliente.id}`, cliente);
        
        if(data.status === 200){
          let clientesCopy = [...clientes];
          let index = clientes.findIndex(c => c.id === cliente.id);
          clientesCopy[index] = data.cliente;
          setClientes(clientesCopy);
          setCliente({
            id: '',
            nombre: '',
            apellidos: ''
          });
          Swal.fire({
            icon: 'success',
            title: 'Cliente actualizado correctamente.',
            confirmButtonText: 'Aceptar!'
          });
        }
        else if(data.status === 500) {
          Swal.fire({
            icon: 'error',
            title: data.message,
          });
        }
      }

      setLoading(false);
    }
  }
  

  return(
    <>
      {
        loading &&
        <Loader loading={loading}/>
      }
      {
        !loading &&
        <Container>
        <Row>
          <Col lg={4}>
              <Card className="mt-5">
                <Card.Header as="h5">Sección clientes</Card.Header>
                <Card.Body>
                  <Card.Title>
                    {
                      !cliente.id ? 'Agregar nuevo registro' : 'Editar registro'
                    }
                  </Card.Title>
                    <Form autoComplete="off">
                      <Form.Group>
                        <Form.Label>Nombre</Form.Label>
                        <Form.Control name="nombre" type="text" placeholder="Nombre" value={cliente.nombre} onChange={handleChange}/>
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Apellidos</Form.Label>
                        <Form.Control name="apellidos" type="text" placeholder="Apellidos" value={cliente.apellidos} onChange={handleChange}/>
                      </Form.Group>

                      { 
                        errors &&
                        <Alert variant="danger">
                          {errors}
                        </Alert>
                      }

                      <Button variant="outline-primary" onClick={() => handleSubmit()}>
                        {
                          !cliente.id ? 'Guardar' : 'Actualizar'
                        }
                      </Button>

                        {
                          cliente.id &&
                          <Button variant="outline-danger" className="float-right" onClick={() => cancelEdit()}>Cancelar</Button>
                        }
                    </Form>
                </Card.Body>
              </Card>
            </Col>

            <Col lg={8}>
              <Table responsive className="mt-5">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                {
                      clientes?.map((cliente, key) => {
                        return <tr key={key}>
                                <td>{cliente.nombre}</td>
                                <td>{cliente.apellidos}</td>
                                <td align="center">
                                  <ButtonGroup size="sm">
                                    <Button variant="outline-warning" onClick={() => selectCliente(cliente.id)}>Editar</Button>
                                    <Button variant="outline-danger" onClick={() => handleDelete(cliente.id)}>Eliminar</Button>
                                  </ButtonGroup>
                                </td>
                              </tr>
                      })
                    }
                  
                </tbody>
              </Table>
            </Col>
          </Row>
        </Container>
      }
    </>
  );
}

export default Cliente;